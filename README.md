# BankStore Example app

Example Android application written using Kotlin and JetPack libraries.
Stores Bank accounts and their associated Cards.

For the app to work, it must be pointed to the backend server.
By default it expects the backend server to be located at `192.168.1.2:8080`.
You can change this value in the `ApiFactory.kt` file.

TODO:
- [ ] Add proper login fragment and flow (right now, username and password are "hardcoded" as default values in SharedPreferences)
- [ ] Add settings screen.
- [ ] Add editable name field to Card update view (all DTOs and entities are ready for this, even on backend).
- [ ] Better UX by making the app offline friendly.
 
 
## Screenshots

![](screen_banks.png)

![](screen_cards.png)