package com.example.bankstoreexample.ui

import android.content.Context
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.BindingAdapter

@BindingAdapter("onEditorEnterAction")
fun EditText.onEditorEnterAction(f: Function1<String, Unit>?) {
    if (f == null) {
        setOnEditorActionListener(null)
    } else {
        setOnEditorActionListener { v, actionId, event ->
            val imeAction = when (actionId) {
                EditorInfo.IME_ACTION_DONE, EditorInfo.IME_ACTION_GO, EditorInfo.IME_ACTION_NEXT -> true
                else -> false
            }

            val keyCodeIsEnter =  event?.keyCode == KeyEvent.KEYCODE_ENTER
            val keyActionIsDown = event?.action == KeyEvent.ACTION_DOWN
            val keydownEvent = keyCodeIsEnter && keyActionIsDown

            if(imeAction or keydownEvent) {
                hideKeyboard(v)
                true.also { f(v.editableText.toString()) }
            } else {
                false
            }
        }
    }


}

private fun hideKeyboard(v: View) {
    val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(v.windowToken, 0)
}