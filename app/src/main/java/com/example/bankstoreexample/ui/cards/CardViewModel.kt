package com.example.bankstoreexample.ui.cards

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.bankstoreexample.data.CardDTO
import com.example.bankstoreexample.data.CardRepository
import com.example.bankstoreexample.ui.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.lang.Exception

private val log = KotlinLogging.logger {  }

class CardViewModel(application: Application, private val repository: CardRepository) :
    AndroidViewModel(application) {

    private val _error: MutableLiveData<Event<Throwable>> = MutableLiveData()
    val error: LiveData<Event<Throwable>>
        get() = _error

    fun getCardsForBankId(bankId: Long): LiveData<List<CardDTO>> =
        repository.getCardsForBankId(bankId)

    fun insert(card: CardDTO) = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.insert(card)
        } catch (e: Exception) {
            log.error(e) { "Could not insert card '$card'" }
            _error.postValue(Event(e))
        }
    }
}