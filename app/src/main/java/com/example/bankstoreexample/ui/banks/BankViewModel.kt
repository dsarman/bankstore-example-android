package com.example.bankstoreexample.ui.banks

import android.app.Application
import androidx.lifecycle.*
import com.example.bankstoreexample.data.BankDTO
import com.example.bankstoreexample.data.BankRepository
import com.example.bankstoreexample.ui.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mu.KotlinLogging

private val log = KotlinLogging.logger { }

class BankViewModel(application: Application, private val repository: BankRepository) :
    AndroidViewModel(application) {

    private val _error: MutableLiveData<Event<Throwable>> = MutableLiveData()
    val error: LiveData<Event<Throwable>>
        get() = _error
    private val _allBanks: MediatorLiveData<List<BankDTO>> = MediatorLiveData()
    private val editableBank: MutableLiveData<BankDTO?> = MutableLiveData(null)

    init {
        _allBanks.addSource(repository.allBanks) { banks ->
            _allBanks.value = banks.map {
                it.editable = it.id == editableBank.value?.id
                it
            }
        }

        // We go through all the banks values, set editable to true if it is the right bank, and
        // then reassign the value to force LiveData onChange event.
        _allBanks.addSource(editableBank) { bank ->
            _allBanks.value = _allBanks.value?.map {
                it.editable = it.id == bank?.id
                it
            }
        }
    }

    val allBanks: LiveData<List<BankDTO>>
        get() = _allBanks

    /**
     * Returns a function that updates given banks name.
     * @param bank The bank whose name will be changed when calling the returned method.
     * @return a function that takes a String argument, and updates the given banks name with it.
     */
    fun saveName(bank: BankDTO): Function1<String, Unit> {
        return { name ->
            bank.name = name
            update(bank)
        }
    }

    fun setEditable(bank: BankDTO) {
        editableBank.value = bank
    }

    fun clearEditable() {
        editableBank.value = null
    }

    private fun update(bank: BankDTO) = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.update(bank)
        } catch (e: Exception) {
            log.error(e) { "Exception occurred while trying to update bank '$bank'" }
            _error.postValue(Event(e))
        }
        editableBank.postValue(null)
    }

    fun insert(bank: BankDTO) = viewModelScope.launch(Dispatchers.IO) {
        val id = try {
            repository.insert(bank)
        } catch (e: Exception) {
            log.error(e) { "Exception occurred while trying to insert bank '$bank'" }
            _error.postValue(Event(e))
            null
        }

        if (id != null && bank.editable) withContext(Dispatchers.Main) {
            editableBank.value = bank.copy(id = id)
        }
    }

    fun changePosition(bank: BankDTO, from: Int) = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.changePosition(bank, from)
        } catch (e: Exception) {
            log.error(e) { "Could not change position of bank '$bank' from '$from'" }
            _error.postValue(Event(e))
        }
    }

    fun delete(bank: BankDTO) = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.delete(bank)
        } catch (e: Exception) {
            log.error(e) { "Could not delete bank '$bank'" }
            _error.postValue(Event(e))
        }
    }

    /**
     * Refreshes the data from backend.
     * @param callback Callback that will be executed on Main thread when the refresh finishes.
     */
    fun refresh(callback: () -> Unit = {}) = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.refresh()
        } catch (e: Exception) {
            log.error(e) { "Could not refresh data from backend" }
            _error.postValue(Event(e))
        }
        withContext(Dispatchers.Main) {
            callback()
        }
    }

}
