package com.example.bankstoreexample.ui.banks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.airbnb.epoxy.EpoxyTouchHelper
import com.example.bankstoreexample.BankCardBindingModel_
import com.example.bankstoreexample.R
import com.example.bankstoreexample.bankCard
import com.example.bankstoreexample.data.BankDTO
import com.example.bankstoreexample.ui.NavigationCommand
import com.example.bankstoreexample.ui.NavigationViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.banks_fragment.*
import mu.KotlinLogging
import org.koin.androidx.viewmodel.ext.android.viewModel

private val log = KotlinLogging.logger { }

class BanksFragment : Fragment() {

    private val viewModel by viewModel<BankViewModel>()
    private val navigationViewModel by viewModel<NavigationViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activity?.title = getString(R.string.banks_fragment_title)
        return inflater.inflate(R.layout.banks_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.allBanks.observe(this, Observer { banks ->
            banks?.let { setRecyclerViewData(banks) }
        })

        viewModel.error.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                view?.let {
                    Snackbar.make(it, getString(R.string.error_snackbar), Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        })

        navigationViewModel.navigationEvent.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                when (it) {
                    is NavigationCommand.ToBank -> {
                        it.bank.id?.let { bankId ->
                            val action = BanksFragmentDirections.actionBankToCard(bankId, it.bank.name)
                            findNavController().navigate(action)
                        }
                    }
                    else -> log.warn { "Navigation command '$it' not supported by this fragment" }
                }
            }
        })

        initEpoxyDraggableBehavior()

        banks_swipe_refresh.setOnRefreshListener {
            viewModel.refresh {
                banks_swipe_refresh.isRefreshing = false
            }
        }

        banks_fab?.setOnClickListener {
            val newItemOrder = viewModel.allBanks.value?.size ?: return@setOnClickListener
            viewModel.insert(BankDTO(null, "", newItemOrder, true))
        }
    }

    /**
     * Sets up draggable behavior in Epoxy that reflects the position changes in database
     */
    private fun initEpoxyDraggableBehavior() {
        bank_recycler_view?.withModels {
            EpoxyTouchHelper.initDragging(this)
                .withRecyclerView(bank_recycler_view)
                .forVerticalList()
                .withTarget(BankCardBindingModel_::class.java)
                .andCallbacks(object : EpoxyTouchHelper.DragCallbacks<BankCardBindingModel_>() {
                    override fun onModelMoved(
                        fromPosition: Int,
                        toPosition: Int,
                        modelBeingMoved: BankCardBindingModel_?,
                        itemView: View?
                    ) {
                        log.debug { "Bank card '$modelBeingMoved' moved from position '$fromPosition' to '$toPosition'" }
                        modelBeingMoved?.bank()?.let {
                            val changedBank = it.copy(position = toPosition)
                            viewModel.changePosition(changedBank, fromPosition)
                        }
                    }

                })
        }
    }

    /**
     * Uses Epoxy kotlin extensions https://github.com/airbnb/epoxy/wiki/EpoxyRecyclerView#kotlin-extensions
     * to set the model data without needing to create additional controller
     */
    private fun setRecyclerViewData(banks: List<BankDTO>) {
        bank_recycler_view?.withModels {
            for (bank in banks) {
                bankCard {
                    id(bank.id)
                    bank(bank.copy())
                    viewModel(viewModel)
                    navigationViewModel(navigationViewModel)
                    // For some reason the recycler view would not be updated if we used this property
                    // directly from the bank object in XML, so we need to set it separately.
                    editable(bank.editable)
                }
            }
        }
    }
}
