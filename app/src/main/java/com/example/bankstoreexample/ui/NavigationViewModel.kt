package com.example.bankstoreexample.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.example.bankstoreexample.data.BankDTO

/**
 * ViewModel that handles all navigation commands.
 * Needed to use navigation commands from data binding XML layouts.
 */
class NavigationViewModel(application: Application) : AndroidViewModel(application) {
    private val _navigationEvent = MutableLiveData<Event<NavigationCommand>>()

    val navigationEvent: LiveData<Event<NavigationCommand>>
        get() = _navigationEvent

    /**
     * Navigates to a [com.example.bankstoreexample.ui.cards.CardsFragment] for the given bank.
     * @param bank The bank whose cards will be displayed in the fragment.
     */
    fun navigateToBank(bank: BankDTO) {
        _navigationEvent.postValue(Event(NavigationCommand.ToBank(bank)))
    }

    /**
     * General function that navigates to given directions.
     * @param directions Destination directions.
     */
    fun navigate(directions: NavDirections) {
        _navigationEvent.postValue(Event(NavigationCommand.To(directions)))
    }

    fun navigateBack() {
        _navigationEvent.postValue(Event(NavigationCommand.Back))
    }
}