package com.example.bankstoreexample.ui.cards

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.bankstoreexample.R
import com.example.bankstoreexample.cardCard
import com.example.bankstoreexample.data.CardDTO
import com.example.bankstoreexample.ui.NavigationCommand
import com.example.bankstoreexample.ui.NavigationViewModel
import kotlinx.android.synthetic.main.cards_fragment.*
import mu.KotlinLogging
import org.koin.androidx.viewmodel.ext.android.viewModel

private val log = KotlinLogging.logger { }

class CardsFragment : Fragment() {

    private val viewModel by viewModel<CardViewModel>()
    private val navigationViewModel by viewModel<NavigationViewModel>()
    private val args by navArgs<CardsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = args.title
        return inflater.inflate(R.layout.cards_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getCardsForBankId(args.bankId).observe(this, Observer { cards ->
            setRecyclerViewData(cards)
        })

        navigationViewModel.navigationEvent.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                when (it) {
                    is NavigationCommand.To -> findNavController().navigate(it.directions)
                    else -> log.error { "Navigation command '$it' is not allowed in this fragment" }
                }
            }
        })


        cards_fab?.setOnClickListener {
            /**
             * This could be done directly, but to maintain consistency, we do all navigation
             * through the [NavigationViewModel].
             */
            val directions = CardsFragmentDirections
                .actionCardsFragmentToCardUpdateFragment(args.bankId)
            navigationViewModel.navigate(directions)
        }
    }

    private fun setRecyclerViewData(cards: List<CardDTO>) {
        cards_recycler_view.withModels {
            for (card in cards) {
                cardCard {
                    id(card.id)
                    card(card)
                    viewModel(viewModel)
                }
            }
        }
    }
}