package com.example.bankstoreexample.ui.cards


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.bankstoreexample.R
import com.example.bankstoreexample.data.CardDTO
import com.example.bankstoreexample.ui.NavigationCommand
import com.example.bankstoreexample.ui.NavigationViewModel
import com.google.android.material.snackbar.Snackbar
import com.manojbhadane.PaymentCardView
import kotlinx.android.synthetic.main.card_update_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.threeten.bp.YearMonth


class CardUpdateFragment : Fragment() {

    private val viewModel by viewModel<CardViewModel>()
    private val navigationViewModel by viewModel<NavigationViewModel>()
    private val args by navArgs<CardUpdateFragmentArgs>()
    private val whiteSpaceRegex = "\\s".toRegex()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = getString(R.string.card_insert_title)
        return inflater.inflate(R.layout.card_update_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        navigationViewModel.navigationEvent.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                if (it is NavigationCommand.Back) {
                    findNavController().popBackStack()
                }
            }
        })

        viewModel.error.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                view?.let {
                    Snackbar.make(it, getString(R.string.error_snackbar), Snackbar.LENGTH_LONG)
                }
            }
        })

        update_card_view.setOnPaymentCardEventListener(object :
            PaymentCardView.OnPaymentCardEventListener {
            override fun onCardDetailsSubmit(
                month: String,
                year: String,
                cardNumber: String,
                cvv: String
            ) {
                val number = cardNumber.replace(whiteSpaceRegex, "").toLong()
                val expiration = YearMonth.of("20$year".toInt(), month.toInt())
                val cvvNumber = cvv.toInt()
                val newCard = CardDTO(null, args.bankId, "", number, expiration, cvvNumber)

                viewModel.insert(newCard)
                navigationViewModel.navigateBack()
            }

            override fun onError(error: String?) {
                view?.let {
                    Snackbar.make(it, getString(R.string.card_input_error_snack), Snackbar.LENGTH_LONG)
                        .show()
                }
            }

            override fun onCancelClick() {
                navigationViewModel.navigateBack()
            }

        })
    }
}
