package com.example.bankstoreexample.ui

import androidx.navigation.NavDirections
import com.example.bankstoreexample.data.BankDTO

sealed class NavigationCommand {
    data class ToBank(val bank: BankDTO): NavigationCommand()
    data class To(val directions: NavDirections): NavigationCommand()
    object Back: NavigationCommand()
}