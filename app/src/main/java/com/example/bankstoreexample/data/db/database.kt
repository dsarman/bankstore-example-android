package com.example.bankstoreexample.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Bank::class, Card::class], version = 1)
@TypeConverters(Converters::class)
abstract class BankStoreRoomDatabase : RoomDatabase() {
    abstract fun bankDao(): BankDao
    abstract fun cardDao(): CardDao
}
