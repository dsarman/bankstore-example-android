package com.example.bankstoreexample.data.db

import android.annotation.SuppressLint
import androidx.room.TypeConverter
import mu.KotlinLogging
import org.threeten.bp.YearMonth

private val log = KotlinLogging.logger { }

@SuppressLint("NewApi")
class Converters {
    @TypeConverter
    fun fromYearMonth(value: YearMonth?): String? {
        return value?.let { value.toString() }
    }

    @TypeConverter
    fun toYearMonth(value: String?): YearMonth? {
        return value?.let { YearMonth.parse(value) }
    }
}