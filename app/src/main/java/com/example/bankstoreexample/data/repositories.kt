package com.example.bankstoreexample.data

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.bankstoreexample.api.BankAndCardsApiRepository
import com.example.bankstoreexample.data.db.BankDao
import com.example.bankstoreexample.data.db.CardDao

/**
 * These repository classes handle all interaction with data, updates both the remote backend
 * storage (REST) and local database (Room - SQLite).
 */


class BankRepository(
    private val bankDao: BankDao,
    private val apiRepository: BankAndCardsApiRepository,
    private val cardRepository: CardRepository
) {
    val allBanks: LiveData<List<BankDTO>> =
        Transformations.map(Transformations.distinctUntilChanged(bankDao.getAllBanks())) { banks ->
            banks.map { it.toDTO() }
        }

    @WorkerThread
    suspend fun insert(bank: BankDTO): Long {
        val bankEntity = bank.toBank()
        val newId = bankDao.insertBank(bankEntity)

        val apiDTO = bank.toBankAndCardsDTO(id = newId)
        apiRepository.save(apiDTO)
        return newId
    }


    @WorkerThread
    suspend fun update(bank: BankDTO) {
        bankDao.updateBanks(bank.toBank())
        cardRepository.saveBankIdAndCards(bank.id!!)
    }

    @WorkerThread
    suspend fun changePosition(bank: BankDTO, from: Int) {
        bankDao.changePosition(bank.toBank(), from)
    }

    @WorkerThread
    suspend fun delete(bank: BankDTO) {
        apiRepository.delete(bank.id!!)
        bankDao.deleteBanks(bank.toBank())
    }

    @WorkerThread
    suspend fun refresh() {
        val banks = apiRepository.list()

        //TODO refactor this to be smarter that delete all & insert.
        bankDao.deleteAll()
        cardRepository.deleteAll()
        bankDao.insertBanksAndCards(banks, cardRepository)
    }
}

class CardRepository(
    private val cardDao: CardDao,
    private val bankDao: BankDao,
    private val apiRepository: BankAndCardsApiRepository
) {
    /**
     * Returns LiveData with Cards for given bank Id.
     */
    fun getCardsForBankId(bankId: Long): LiveData<List<CardDTO>> =
        Transformations.map(
            Transformations.distinctUntilChanged(
                cardDao.getCardsForBankIdLive(
                    bankId
                )
            )
        ) { cards ->
            cards.map { it.toDTO() }
        }

    @WorkerThread
    suspend fun insert(card: CardDTO): Long {
        val newId = cardDao.insertCard(card.toCard())

        /**
         * Need to do this since backend has only one endpoint able to save entities, and that is
         * for a whole bank entity.
         */
        saveBankIdAndCards(card.bankId!!)
        return newId
    }

    @WorkerThread
    suspend fun deleteAll() {
        cardDao.deleteAll()
    }

    @WorkerThread
    suspend fun insertCards(vararg cards: CardDTO) {
        cardDao.insertCards(*cards.map { it.toCard() }.toTypedArray())
    }

    /**
     * POSTs the all data for given bank id to the backend.
     */
    @WorkerThread
    suspend fun saveBankIdAndCards(bankId: Long) {
        val bank = bankDao.findById(bankId) ?: return
        val allCardsForBank = cardDao.getCardsForBankId(bankId)
        val apiDTO = BankAndCardsDTO(bankId, bank.name, allCardsForBank.map { it.toDTO() })

        apiRepository.save(apiDTO)
    }
}