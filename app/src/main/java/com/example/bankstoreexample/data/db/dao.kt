package com.example.bankstoreexample.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.bankstoreexample.data.BankAndCardsDTO
import com.example.bankstoreexample.data.CardRepository
import mu.KotlinLogging

private val log = KotlinLogging.logger { }

@Dao
interface BankDao {
    @Query("SELECT * from Bank ORDER BY position ASC")
    fun getAllBanks(): LiveData<List<Bank>>

    @Query("SELECT * FROM Bank WHERE id = :id")
    fun findById(id: Long): Bank?

    @Insert
    suspend fun insertBank(bank: Bank): Long

    /**
     * Returns the maximum position number.
     */
    @Query("SELECT MAX(position) FROM Bank GROUP BY id")
    suspend fun getLastPosition(): Int?

    /**
     * WARNING:
     * If you are updating the banks order, you must use the {@see #changePosition()} function
     */
    @Update
    suspend fun updateBanks(vararg banks: Bank)

    /**
     * Raises the position of all banks on and after this position by one (to make room for reordered item)
     * WARNING: Should not be used outside of the DAO, but needs to be public for Room to work
     */
    @Query("UPDATE Bank SET position = position + :change WHERE position >= :from AND position <= :to")
    suspend fun slidePositions(from: Int, to: Int, change: Int)


    /**
     * Changes banks position.
     * @param bank The bank whose position was changed. The changed position must be already set on the object.
     * @param from The original position the bank was located at.
     */
    @Transaction
    suspend fun changePosition(bank: Bank, from: Int) {
        if (from == bank.position) {
            log.warn {
                "Got a no-op change position, this should not happen as Epoxy drag helper" +
                        "should not send empty position changes"
            }
            return
        }
        if (from < bank.position) {
            slidePositions(from + 1, bank.position, -1)
        } else {
            slidePositions(bank.position, from - 1, +1)
        }
        updateBanks(bank)
    }

    @Transaction
    suspend fun insertBanksAndCards(
        banksAndCardsDTO: List<BankAndCardsDTO>,
        cardRepository: CardRepository
    ) {
        var lastPosition = getLastPosition() ?: -1

        for (bankAndCardsDTO in banksAndCardsDTO) {
            lastPosition += 1
            val bank = Bank(null, bankAndCardsDTO.name, lastPosition)
            val newBankId = insertBank(bank)
            val cards = bankAndCardsDTO.cards.map { it.copy(bankId = newBankId) }

            cardRepository.insertCards(*cards.toTypedArray())
        }
    }


    /**
     * Deletes banks, and also takes care of adjusting position if there were any banks positioned
     * after the one being removed, so no gaps are created.
     */
    @Transaction
    suspend fun deleteBanks(vararg banks: Bank) {
        deleteBanksQuery(*banks)
        banks.forEach { slidePositions(it.position, Int.MAX_VALUE, -1) }
    }

    /**
     * WARNING: Should not be used outside of the DAO, but needs to be public for Room to work.
     */
    @Delete
    suspend fun deleteBanksQuery(vararg banks: Bank)

    @Query("DELETE FROM Bank")
    fun deleteAll()
}

private const val SELECT_BY_BANK_ID_QUERY = "SELECT * FROM Card WHERE bank_id = :bankId"

@Dao
interface CardDao {
    @Query("SELECT * FROM Card")
    fun getAllCards(): LiveData<List<Card>>

    @Query(SELECT_BY_BANK_ID_QUERY)
    fun getCardsForBankIdLive(bankId: Long): LiveData<List<Card>>

    @Query(SELECT_BY_BANK_ID_QUERY)
    suspend fun getCardsForBankId(bankId: Long): List<Card>

    @Insert
    suspend fun insertCard(card: Card): Long

    @Insert
    suspend fun insertCards(vararg cards: Card)

    @Query("DELETE FROM Card")
    suspend fun deleteAll()
}