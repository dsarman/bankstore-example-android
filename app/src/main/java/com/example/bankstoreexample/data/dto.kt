package com.example.bankstoreexample.data

import com.example.bankstoreexample.data.db.Bank
import com.example.bankstoreexample.data.db.Card
import com.squareup.moshi.Json
import org.threeten.bp.YearMonth

data class BankDTO(
    val id: Long?,
    var name: String,
    var position: Int,
    var editable: Boolean
) {
    fun toBank() = Bank(id, name, position)
    fun toBankAndCardsDTO(id: Long = this.id!!) = BankAndCardsDTO(id, name, arrayListOf())
}

fun Bank.toDTO(isEdited: Boolean = false) = BankDTO(id, name, position, isEdited)


data class CardDTO(
    @Json(name = "appId")
    val id: Long?,
    val bankId: Long?,
    var name: String,
    var number: Long?,
    val expiration: YearMonth?,
    val ccv: Int?
) {
    fun toCard(bankId: Long = this.bankId!!) = Card(id, bankId, name, number, expiration, ccv)
    fun yearEnd() = expiration?.year.toString().takeLast(2)
}

fun Card.toDTO() = CardDTO(id, bankId, name, number, expiration, ccv)


data class BankAndCardsDTO(
    var appId: Long,
    var name: String,
    var cards: List<CardDTO> = arrayListOf()
)