package com.example.bankstoreexample.data.db

import androidx.room.*
import org.threeten.bp.YearMonth


@Entity
data class Bank(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val name: String,
    val position: Int
)


@Entity(
    foreignKeys = [ForeignKey(
        entity = Bank::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("bank_id"),
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index(
        name = "bank_id_index", value = ["bank_id"]
    )]
)
data class Card(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    @ColumnInfo(name = "bank_id") val bankId: Long,
    val name: String,
    val number: Long?,
    val expiration: YearMonth?,
    val ccv: Int?
)

