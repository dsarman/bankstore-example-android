package com.example.bankstoreexample

import com.chibatching.kotpref.KotprefModel

object UserInfo : KotprefModel() {
    var username by stringPref(default = "user1")
    var password by stringPref(default = "user1")

    val isLoggedIn: Boolean
        get() = username.isNotEmpty() && password.isNotEmpty()
}