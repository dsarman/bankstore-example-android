package com.example.bankstoreexample.api

import com.example.bankstoreexample.data.BankAndCardsDTO
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*


interface BankAndCardsApiService {
    @GET("/list")
    fun listAsync(): Deferred<Response<List<BankAndCardsDTO>>>

    @GET("/detail/{id}")
    fun detailAsync(@Path("id") id: Long): Deferred<Response<BankAndCardsDTO>>

    @POST("/save")
    fun saveAsync(@Body body: BankAndCardsDTO): Deferred<Response<Boolean>>

    @DELETE("/remove/{id}")
    fun deleteAsync(@Path("id") id: Long): Deferred<Response<Boolean>>
}


/**
 * Returns response body if the request was successful, throws an exception if it was not
 * @param response Response object to be checked
 * @param errorHandler Optional custom error handler, which receives the http status code,
 *                     and return a throwable that will be throw
 */
fun <T> checkResponse(response: Response<T>?, errorHandler: (Int) -> Throwable? = { null }): T {
    when {
        response == null -> throw UnsuccessfulApiRequestException("Request could not be made")
        response.isSuccessful -> return response.body()
            ?: throw UnsuccessfulApiRequestException("Api request body was null")
        else -> {
            val throwable = errorHandler(response.code())
                ?: throw UnsuccessfulApiRequestException("Api request failed with http code '${response.code()}'")
            throw throwable
        }
    }
}


class BankAndCardsApiRepository(private val apiService: BankAndCardsApiService) {
    suspend fun list(): List<BankAndCardsDTO> {
        val banksResponse = apiService.listAsync().await()
        return checkResponse(banksResponse)
    }

    suspend fun detail(id: Long): BankAndCardsDTO {
        val bankResponse = apiService.detailAsync(id).await()
        return checkResponse(bankResponse)
    }

    suspend fun delete(id: Long): Boolean {
        val response = apiService.deleteAsync(id).await()
        return checkResponse(response)
    }

    suspend fun save(body: BankAndCardsDTO): Boolean {
        val response = apiService.saveAsync(body).await()
        return checkResponse(response)
    }
}
