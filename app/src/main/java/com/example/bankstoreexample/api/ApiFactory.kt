package com.example.bankstoreexample.api

import com.example.bankstoreexample.UserInfo
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


private const val API_URL = "http://192.168.1.2:8080"

/**
 * Helper object used to create Retrofit client.
 */
object ApiFactory {

    /**
     * OkHttp Interceptor used to add basic auth credentials to all requests.
     */
    private val authInterceptor = Interceptor { chain ->
        val newHeader = chain.request()
            .newBuilder()
            .header("Authorization", Credentials.basic(UserInfo.username, UserInfo.password))
            .build()

        chain.proceed(newHeader)
    }

    /**
     * OkHttp client used for rest calls.
     */
    private val apiClient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .build()

    /**
     * Moshi JSON library initialization.
     */
    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .add(JsonTypeAdapter())
        .build()

    /**
     * Retrofit client used for REST communication with backend.
     */
    fun retrofit(): Retrofit = Retrofit.Builder()
        .client(apiClient)
        .baseUrl(API_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}