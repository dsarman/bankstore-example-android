package com.example.bankstoreexample.api

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.ToJson
import org.threeten.bp.DateTimeException
import org.threeten.bp.YearMonth

class JsonTypeAdapter {
    @ToJson fun toJson(yearMonth: YearMonth): String {
        return yearMonth.toString()
    }

    @FromJson fun fromJson(yearMonth: String): YearMonth {
        try {
            return YearMonth.parse(yearMonth)
        } catch (e: DateTimeException) {
            throw JsonDataException("Could not parse the YearMonth string '$yearMonth'", e)
        }
    }
}