package com.example.bankstoreexample.api


class UnsuccessfulApiRequestException(override val message: String) : Exception(message)
