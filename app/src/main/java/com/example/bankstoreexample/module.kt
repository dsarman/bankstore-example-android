package com.example.bankstoreexample

import androidx.room.Room
import com.example.bankstoreexample.api.ApiFactory
import com.example.bankstoreexample.api.BankAndCardsApiRepository
import com.example.bankstoreexample.api.BankAndCardsApiService
import com.example.bankstoreexample.data.BankRepository
import com.example.bankstoreexample.data.CardRepository
import com.example.bankstoreexample.data.db.BankStoreRoomDatabase
import com.example.bankstoreexample.ui.NavigationViewModel
import com.example.bankstoreexample.ui.banks.BankViewModel
import com.example.bankstoreexample.ui.cards.CardViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    viewModel { BankViewModel(androidApplication(), get()) }
    viewModel { CardViewModel(androidApplication(), get()) }
    viewModel { NavigationViewModel(androidApplication()) }

    // Room database itself
    single(createdAtStart = true) {
        Room.databaseBuilder(
            androidApplication(),
            BankStoreRoomDatabase::class.java,
            ROOM_DATABASE_NAME
        ).build()
    }

    // BankDao
    single {
        val roomDb: BankStoreRoomDatabase = get()
        roomDb.bankDao()
    }

    // CardDao
    single {
        val roomDb: BankStoreRoomDatabase = get()
        roomDb.cardDao()
    }

    single { BankRepository(get(), get(),get()) }
    single { CardRepository(get(), get(), get()) }

    single { ApiFactory.retrofit().create(BankAndCardsApiService::class.java) }
    single { BankAndCardsApiRepository(get())}
}
